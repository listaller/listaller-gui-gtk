#ifndef CONFIG_H
#define CONFIG_H

#cmakedefine PROJECT_NAME "@PROJECT_NAME@"
#cmakedefine PROJECT_VERSION "@PROJECT_VERSION@"
#cmakedefine PREFIXDIR "@PREFIXDIR@"
#cmakedefine DATADIR "@DATADIR@"
#cmakedefine PKGDATADIR "@PKGDATADIR@"
#cmakedefine LOCALEDIR "@LOCALE_DIR@"
#cmakedefine BUILDDIR "@BUILDDIR@"
#cmakedefine GETTEXT_PACKAGE "@GETTEXT_PACKAGE@"

#endif // CONFIG_H
