# CMakeLists for Listaller-GTK+ XML user interface data

set(LIGTK_UI_FILES
	install-wizard.ui
	app-manager.ui
)

install(FILES ${LIGTK_UI_FILES} DESTINATION ${DATADIR}/ui)
