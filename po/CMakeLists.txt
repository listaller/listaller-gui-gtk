# CMakeLists for Listaller-GTK+-GUI translation

find_program (LC_INTLTOOL_UPDATE intltool-update)
mark_as_advanced (LC_INTLTOOL_UPDATE)

find_package (Gettext REQUIRED)

if (LC_INTLTOOL_UPDATE STREQUAL "")
	message (FATAL_ERROR "Intltool was not found! Please install it to continue!")
endif (LC_INTLTOOL_UPDATE STREQUAL "")

add_custom_target (nls-update
	${LC_INTLTOOL_UPDATE} -p -g "${PROJECT_NAME}" -o "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.pot"
	DEPENDS POTFILES.in
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)

file (GLOB _translations ${CMAKE_CURRENT_SOURCE_DIR}/*.po)
        gettext_create_translations (${PROJECT_NAME}.pot
				     ALL ${_translations}
	)
