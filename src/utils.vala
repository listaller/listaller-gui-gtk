/* utils.vala
 *
 * Copyright (C) 2011-2012 Matthias Klumpp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 * 	Matthias Klumpp <matthias@tenstral.net>
 */

using GLib;
using Gee;

namespace LiGtk.Utils {

// Only for testing!
static const bool use_local_datadir = true;

string find_datadir (string[] args, string data_file = "install-wizard.ui") {
	string exedir = Path.get_dirname (args[0]);
	string datadir = "";
	if (use_local_datadir) {
		if (FileUtils.test (Path.build_filename (exedir, "..", "..",
		    "data", "ui", data_file, null), FileTest.EXISTS)) {
			datadir = Path.build_filename (exedir, "..", "..", "data", null);
			message ("Using local data directory!");
			return datadir;
		}
	}

	datadir = Config.DATADIR;
	if (!FileUtils.test (Path.build_filename (datadir, "ui", data_file, null), FileTest.EXISTS))
		critical ("Important application data was not found!");
	return datadir;
}

}
