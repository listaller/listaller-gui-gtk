/* appicon.vala
 *
 * Copyright (C) 2011-2012 Matthias Klumpp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 * 	Matthias Klumpp <matthias@tenstral.net>
 */

using GLib;
using Gtk;
using Gdk;
using Listaller;

public class AppIcon {
	private AppItem app;
	private int iconSize;

	public AppIcon (AppItem appID) {
		app = appID;
		iconSize = 48;
	}

	private Pixbuf get_default_icon () {
		IconTheme itheme = IconTheme.get_default ();

		Pixbuf? pix = null;
		try {
			pix = itheme.load_icon ("applications-other", iconSize,
							IconLookupFlags.GENERIC_FALLBACK);
		} catch (Error e) {
			pix = null;
		}

		if (pix == null)
			pix = new Pixbuf (Colorspace.RGB, true, 8, iconSize, iconSize);
		return pix;
	}

	public Pixbuf get_pixbuf () {
		Pixbuf pix;
		string iconName = app.icon_name;

		if (iconName == "")
			return get_default_icon ();

		if (iconName.has_prefix ("/")) {
			// Return default icon if icon file does not exists
			if (!FileUtils.test (iconName, FileTest.EXISTS))
				return get_default_icon ();

			// We need to load the icon from a file
			try {
				pix = new Gdk.Pixbuf.from_file (iconName);
			} catch (Error e) {
				warning (_("Unable to load icon %s! Message: %s").printf (iconName, e.message));
				return get_default_icon ();
			}
			int dest_width = pix.width - (pix.height - iconSize);
			pix = pix.scale_simple (dest_width, iconSize, InterpType.BILINEAR);
			return pix;
		}

		// If we're here, we need to load an icon from stock
		IconTheme itheme = IconTheme.get_default ();
		try {
			pix = itheme.load_icon (iconName, iconSize, IconLookupFlags.GENERIC_FALLBACK);
		} catch (Error e) {
			warning (_("Skipping icon for %s: %s").printf (app.full_name, e.message));
			return get_default_icon ();
		}
		return pix;
	}
}
