# CMakeLists for Listaller GTK+ Frontend

find_package(Vala REQUIRED)
include("${VALA_USE_FILE}")

find_package(GLIB2 REQUIRED)
find_package(Gettext REQUIRED)
pkg_check_modules(GTK3 REQUIRED gtk+-3.0)
pkg_check_modules(GEE REQUIRED gee-1.0)
pkg_check_modules(LISTALLER REQUIRED listaller-glib)

set (LISETUPGTK_SOURCES utils.vala
			li-setup.vala
)

set (LIMANAGERGTK_SOURCES utils.vala
			li-manager.vala
			appicon.vala
			cellrenderer-uri.vala
)

include_directories(${CMAKE_BINARY_DIR}
		    ${CMAKE_CURRENT_SOURCE_DIR}
		    ${GLIB2_INCLUDE_DIR}
		    ${GEE_INCLUDE_DIRS}
		    ${GTK3_INCLUDE_DIRS}
		    ${LISTALLER_INCLUDE_DIRS}
		    /usr/include/listaller-glib
)

if (NOT ${GTK3_FOUND})
	message(FATAL_ERROR "GTK3 was not found! Please install GTK3 to continue!")
endif ()

# Listaller Setup GUI
vala_add_executable (lisetup-gtk ${LISETUPGTK_SOURCES}
				PACKAGES config glib-2.0 listaller-0.5 gee-1.0 gtk+-3.0
				VAPI_DIRS ${CMAKE_SOURCE_DIR}/vapi
				COMPILE_FLAGS --thread
)

target_link_libraries(lisetup-gtk
		${GLIB2_LIBRARIES}
		${GEE_LIBRARIES}
		${GTK3_LIBRARIES}
		${LISTALLER_LIBRARIES}
)

install(TARGETS lisetup-gtk DESTINATION bin)

# Listaller AppManager GUI
vala_add_executable (limanager-gtk ${LIMANAGERGTK_SOURCES}
				PACKAGES config glib-2.0 listaller-0.5 gee-1.0 gtk+-3.0
				VAPI_DIRS ${CMAKE_SOURCE_DIR}/vapi
				COMPILE_FLAGS --thread
)

target_link_libraries(limanager-gtk
		${GLIB2_LIBRARIES}
		${GEE_LIBRARIES}
		${GTK3_LIBRARIES}
		${LISTALLER_LIBRARIES}
)

install(TARGETS limanager-gtk DESTINATION bin)
