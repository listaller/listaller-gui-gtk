/* li-manager.vala
 *
 * Copyright (C) 2010-2012 Matthias Klumpp
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 * 	Matthias Klumpp <matthias@tenstral.net>
 */

using GLib;
using Gee;
using Gtk;
using Listaller;

static string datadir;

public enum AppColumn {
	STATE,
	ICON,
	TEXT,
	APPITEM,
	LAST;
}

public enum AppDetailColumn {
	TITLE,
	TEXT,
	URI,
	LAST;
}

public class AppManager : Object {
	private Window win;
	private Builder ui;
	private Manager mgr;
	public int exit_code { get; set; }

	private ListStore apps_store;
	private ListStore details_store;

	public AppManager (bool testmode = false) {
		try {
			ui = new Builder ();
			ui.add_from_file (Path.build_filename (datadir, "ui", "app-manager.ui", null));
			ui.connect_signals (this);
			win = ui.get_object ("mainwindow") as Window;
		} catch (Error e) {
			stdout.printf ("Could not load UI: %s\n", e.message);
			exit_code = 1;
		}

		mgr = new Manager ();
		mgr.message.connect (manager_message);
		mgr.error_code.connect (manager_error_code);
		//! mgr.status_changed.connect (manager_status_changed);
		mgr.progress_changed.connect (manager_progress_changed);
		mgr.application.connect (manager_new_application);

		// Build application list widget
		TreeView appView = get_widget ("treeview_apps") as TreeView;
		apps_store = new ListStore (AppColumn.LAST, typeof(bool), typeof(Gdk.Pixbuf), typeof(string), typeof(AppItem));
		appView.set_model (apps_store);
		add_appview_columns (appView);
		appView.get_selection ().changed.connect (ui_apptreeview_clicked_cb);

		// Build application details widget
		TreeView detailView = get_widget ("treeview_details") as TreeView;
		details_store = new ListStore (AppDetailColumn.LAST, typeof(string), typeof(string), typeof(string));
		detailView.set_model (details_store);
		add_detailview_columns (detailView);
		//detailView.get_selection ().changed.connect (ui_applications_treeview_clicked_cb);

		Button btn = (get_widget ("button_apply") as Button);
		btn.sensitive = false;
		btn.clicked.connect (ui_apply_button_clicked_cb);

		(get_widget ("button_cancel") as Button).clicked.connect (quit_application);

		win.set_default_size (600, 400);
		win.icon_name = "softwarecenter";

		mgr.find_applications (AppOrigin.ALL);
	}

	private Object? get_widget (string name) {
		return ui.get_object (name);
	}

	private void manager_error_code (ErrorItem error) {
		Gtk.MessageDialog d = new Gtk.MessageDialog.with_markup(win, Gtk.DialogFlags.MODAL,
									Gtk.MessageType.ERROR,
									Gtk.ButtonsType.OK, error.details, null);
		d.run();
		d.destroy();
		// quit_application ();
	}

	private void manager_new_application (AppItem app) {
		TreeIter iter;
		apps_store.append (out iter);
		if (app.icon_name == "")
			app.update_with_desktop_file ();

		AppIcon icon = new AppIcon (app);

		string text = "<b>%s</b>".printf (app.full_name);
		if (!app.shared)
			text += " <i>%s</i>".printf (_("(personal)"));
		text += "\n";
		text += "<i>%s</i>".printf (app.summary);

		apps_store.set (iter, AppColumn.STATE, true,
				AppColumn.ICON, icon.get_pixbuf (),
				AppColumn.TEXT, text,
				AppColumn.APPITEM, app);
	}

	private void manager_progress_changed (int progress) {
		//
	}

	private void manager_message (MessageItem message) {
		stdout.printf (message.details + "\n");
	}

	private void add_detailview_item (string title, string? text, string uri = "") {
		// Just in case...
		if (text == null)
			text = "";
		TreeIter iter;

		string markup = "<b>%s:</b>".printf (title);

		details_store.append (out iter);
		details_store.set (iter,
				AppDetailColumn.TITLE, markup,
				AppDetailColumn.TEXT, text,
				AppDetailColumn.URI, uri,
				-1);
	}

	private void ui_apptreeview_clicked_cb (TreeSelection selection) {
		TreeModel model;
		TreeIter iter;
		AppItem? app = null;

		// This will only work in single or browse selection mode!
		if (!selection.get_selected (out model, out iter)) {
			debug ("no row selected");
			return;
		}

		// check we aren't a help line
		model.get (iter, AppColumn.APPITEM, out app, -1);
		if (app == null) {
			debug ("ignoring help click");
			return;
		}

		// We fetch the item again from the DB to get all information (which is not covered in an app-id)
		AppItem? item = mgr.get_application_by_idname (app.idname);
		if (item != null)
			app = item;

		// Cleanup details TreeView
		details_store.clear ();

		//: The application title
		add_detailview_item (_("Title"), app.full_name);
		//: Version of this application
		add_detailview_item (_("Version"), app.version);
		//: The author of this application
		add_detailview_item (_("Author"), app.author);
		//: URL for this application
		add_detailview_item (_("URL"), _("Homepage"), app.website);
		//: The license of this application
		add_detailview_item (_("License"), app.license.name);
		//: The publisher of this package/application
		add_detailview_item (_("Publisher"), app.publisher);
		//: Origin of this application
		add_detailview_item (_("Origin"), app.origin.to_string ());

		// Show description
		TextView? desc_view = get_widget ("textview_info") as TextView;
		assert (desc_view != null);
		desc_view.buffer.text = app.description;
	}

	private void ui_apptreeview_installed_clicked_cb (CellRendererToggle cell, string path_str) {
		TreeView view = get_widget ("treeview_apps") as TreeView;

		TreeModel model = view.get_model ();
		TreePath path = new TreePath.from_string (path_str);

		TreeIter iter;
		bool state;
		model.get_iter (out iter, path);
		model.get (iter, AppColumn.STATE, out state, -1);

		// enforce the selection in case we just fire at the checkbox without selecting
		TreeSelection selection = view.get_selection ();
		selection.select_iter (iter);

		apps_store.set (iter, AppColumn.STATE, !state, -1);

		(get_widget ("button_apply") as Button).sensitive = true;
	}

	private void ui_apply_button_clicked_cb () {
		TreeIter iter;
		apps_store.get_iter_first (out iter);

		//TODO: More security checks and more 'sane' code
		// ...this is just a big hack for testing ;-)
		do {
			bool state;
			AppItem? app;
			apps_store.get (iter, AppColumn.STATE, out state, -1);
			if (!state) {
				apps_store.get (iter, AppColumn.APPITEM, out app, -1);
				if (app != null) {
					debug ("Removing: %s", app.idname);
					mgr.remove_application (app);
					apps_store.remove (iter);
				}
			}
		} while (apps_store.iter_next (ref iter));
	}

	private void add_appview_columns (TreeView view) {
		CellRenderer renderer;
		TreeViewColumn column;

		// column for installed toggles
		renderer = new CellRendererToggle ();
		(renderer as CellRendererToggle).toggled.connect (ui_apptreeview_installed_clicked_cb);
		column = new TreeViewColumn.with_attributes (_("Installed"), renderer,
							"active", AppColumn.STATE, null);
		view.append_column (column);

		// Column for application icons
		renderer = new CellRendererPixbuf ();
		(renderer as CellRendererPixbuf).stock_size = IconSize.DIALOG;
		column = new TreeViewColumn.with_attributes (_("Icon"), renderer,
							"pixbuf", AppColumn.ICON, null);
		view.append_column (column);

		// Column for text contend
		renderer = new CellRendererText ();
		column = new TreeViewColumn.with_attributes (_("Name"), renderer,
							"markup", AppColumn.TEXT, null);
		column.set_sort_column_id (-1);
		view.append_column (column);
	}

	private void add_detailview_columns (TreeView view) {
		CellRenderer renderer;
		TreeViewColumn column;

		column = new TreeViewColumn ();
		renderer = new CellRendererText ();
		column.pack_start (renderer, false);
		column.add_attribute (renderer, "markup", AppDetailColumn.TITLE);
		view.append_column (column);

		// Column for uris
		renderer = new CellRendererUri ();
		//g_signal_connect (renderer, "clicked", G_CALLBACK (gpk_application_treeview_renderer_clicked), NULL);
		column = new TreeViewColumn.with_attributes (_("Text"), renderer,
							"text", AppDetailColumn.TEXT,
							"uri", AppDetailColumn.URI, null);
		view.append_column (column);
		view.columns_autosize ();
	}

	private void quit_application () {
		Gtk.main_quit ();
	}

	public void run () {
		if (exit_code > 0)
			return;

		win.destroy.connect (quit_application);
		win.show ();
		Gtk.main ();
	}

}

public class CmdLnOptions : Object {
	static bool o_show_version = false;
	static bool o_use_testmode = false;
	static bool o_verbose_mode = false;

	private OptionContext opt_context;

	private const OptionEntry[] options = {
		{ "version", 'v', 0, OptionArg.NONE, ref o_show_version,
		N_("Show the application's version"), null },
		{ "testmode", 'v', 0, OptionArg.NONE, ref o_use_testmode,
		N_("Run installation in testmode"), null },
		{ "verbose", 'v', 0, OptionArg.NONE, ref o_verbose_mode,
		N_("Show verbose output"), null },
		{ null }
	};

	public CmdLnOptions () {
		opt_context = new OptionContext ("- application manager.");
	}

	public int process (string[] args) {

		opt_context.set_help_enabled (true);
		opt_context.add_main_entries (options, null);
		try {
			opt_context.parse (ref args);
		} catch (Error e) {
			stdout.printf (e.message + "\n");
			stdout.printf (_("Run '%s --help' to see a list of available command-line options.\n"), args[0]);
			return 1;
		}

		return 0;
	}

	public string get_help () {
		return opt_context.get_help (true, null);
	}

	static int main (string[] args) {
		Gtk.init (ref args);
		// Bind Listaller locale
		Intl.bindtextdomain("listaller", Config.LOCALEDIR);
		Intl.bind_textdomain_codeset("listaller", "UTF-8");
		Intl.textdomain("listaller");
		// Bind Listaller GTK UI locale
		Intl.setlocale(LocaleCategory.ALL,"");
		Intl.bindtextdomain(Config.GETTEXT_PACKAGE, Config.LOCALEDIR);
		Intl.bind_textdomain_codeset(Config.GETTEXT_PACKAGE, "UTF-8");
		Intl.textdomain(Config.GETTEXT_PACKAGE);

		var optn = new CmdLnOptions ();
		int i = optn.process (args);
		if (i > 0)
			return i;

		Listaller.set_console_mode (true);
		Listaller.set_verbose_mode (o_verbose_mode);
		Listaller.add_log_domain ("AppManager");

		if (o_show_version) {
			stdout.printf ("Version: %s\n", Config.PROJECT_VERSION);
			return 0;
		}

		// Find the data directory
		datadir = LiGtk.Utils.find_datadir (args, "app-manager.ui");

		var appManager = new AppManager (o_use_testmode);
		appManager.run ();
		return appManager.exit_code;
	}

}
