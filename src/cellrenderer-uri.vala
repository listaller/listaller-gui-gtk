/* cellrenderer-uri.vala
 *
 * Copyright (C) 2011-2012 Matthias Klumpp
 *                         Richard Hughes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:
 * 	Matthias Klumpp <matthias@tenstral.net>
 */

using GLib;
using Gtk;
using Gdk;

enum CruProp {
	ZERO,
	URI,
	CLICKED
}

public class CellRendererUri : CellRendererText {
	private HashTable<string, bool> clickTab;
	public string uri { get; set; }

	public signal void clicked (string url);

	public CellRendererUri () {
		uri = "";
		clickTab = new HashTable <string, bool> (str_hash, str_equal);
	}

	private bool is_clicked () {
		bool value = clickTab.lookup (uri);
		return value;
	}

	private void set_clicked (bool b) {
		if (b)
			clickTab.insert (uri, true);
		else
			clickTab.remove (uri);
	}

	protected override bool activate (Event event, Widget widget,
				string path, Rectangle background_area,
				Rectangle cell_area, CellRendererState flags) {
		if (uri == "")
			return false;

		set_clicked (true);
		clicked (uri);
		return true;
	}

	protected new void get_property (uint param_id, Value value, ParamSpec pspec) {
		switch (param_id) {
			case CruProp.URI:
				value = uri;
				break;
			case CruProp.CLICKED:
				bool ret = is_clicked ();
				value = ret;
				break;
			default:
				warning ("Invalid ID!");
				break;
		}
	}

	protected new void set_property (uint param_id, Value value, ParamSpec pspec) {
		switch (param_id) {
			case CruProp.URI:
				uri = "";
				uri = value.get_string ();
				break;
			case CruProp.CLICKED:
				bool ret = value.get_boolean ();
				set_clicked (ret);
				break;
			default:
				warning ("Invalid property ID!");
				break;
		}
	}

	private void set_link_rgba (RGBA color, bool visited)
	{
		const double color_half = 0.5;
		const double offset = 1.0 / 3;

		if (visited) {
			if (color.red < color_half && color.blue < color_half) {
				color.red += offset;
				color.blue += offset;
				return;
			}
			if (color.green > color_half) {
				color.green -= offset;
				return;
			}
		} else {
			if (color.blue < color_half) {
				color.blue += offset;
				return;
			}
			if (color.red > color_half && color.green > color_half) {
				color.red -= offset;
				color.green -= offset;
				return;
			}
		}

		debug ("cannot get color for %f,%f,%f", color.red, color.blue, color.green);
	}

	protected override void render (Cairo.Context cr,
				Widget widget,
				Rectangle background_area,
				Rectangle cell_area,
				CellRendererState flags)
	{
		bool ret;
		StyleContext style;
		RGBA rgba;
		// Has to be pointer, workaround for Vala bug (0.14)
		// FIXME
		Color *color = null;
		string color_string;

		ret = is_clicked ();

		/* get a copy of the widget color */
		style = widget.get_style_context ();

		/* set colour */
		if (uri == "") {
			rgba = style.get_color (StateFlags.NORMAL);
			color_string = rgba.to_string ();
			this.foreground = color_string;
			this.underline = Pango.Underline.NONE;
		} else if (ret) {
			/* if we defined this in our theme, else find a compromise */
			style.get_style ("visited-link-color", out color,
				null);
			if (color == null) {
				rgba = style.get_color (StateFlags.NORMAL);
				set_link_rgba (rgba, true);

				color_string = rgba.to_string ();
			} else {
				color_string = color.to_string ();
			}

			this.foreground = color_string;
			this.underline = Pango.Underline.SINGLE;
		} else {
			/* if we defined this in our theme, else find a compromise */
			style.get_style ("link-color", out color,
				null);
			if (color == null) {
				rgba = style.get_color (StateFlags.NORMAL);
				set_link_rgba (rgba, false);

				color_string = rgba.to_string ();
			} else {
				color_string = color.to_string ();
			}

			this.foreground = color_string;
			this.underline = Pango.Underline.SINGLE;
		}

		/* can we select the text or click it? */
		if (uri == "") {
			this.editable = true;
			this.mode = CellRendererMode.EDITABLE;
		} else  {
			this.mode = CellRendererMode.ACTIVATABLE;
		}

		base.render (cr, widget, background_area, cell_area, flags);
	}
}



