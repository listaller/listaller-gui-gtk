[CCode (prefix = "", lower_case_cprefix = "", cheader_filename = "config.h")]
namespace Config
{
	/* Package information */
	public const string PROJECT_NAME;
	public const string PROJECT_VERSION;

	/* Gettext package */
	public const string GETTEXT_PACKAGE;

	/* Configured paths - these variables are not present in config.h, they are
	 * passed to underlying C code as cmd line macros. */
	public const string PREFIXDIR;
	public const string LOCALEDIR;  /* /usr/local/share/locale  */
	public const string DATADIR; /* /usr/local/share/listaller-gtk */
	public const string LIBDIR;  /* /usr/local/lib/   */
}
